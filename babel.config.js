const presets = [
  [
    '@babel/preset-env',
    {
      targets: [
        '>0.25%',
        'not ie 11',
        'not op_mini all',
      ],
      useBuiltIns: 'entry',
      corejs: {
        version: 3,
        proposals: true,
      },
      modules: 'commonjs',
    },
  ],
];

const plugins = [
  '@babel/plugin-transform-runtime',
  '@babel/proposal-class-properties',
];

module.exports = {
  presets,
  plugins,
  exclude: [/\/core-js\//],
};
