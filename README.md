[![Codacy Badge](https://api.codacy.com/project/badge/Grade/f1740d6fc2f446088b22b024e1f93bca)](https://www.codacy.com/app/foeewni/web-core?utm_source=foeewni@bitbucket.org&amp;utm_medium=referral&amp;utm_content=foeewni/web-core&amp;utm_campaign=Badge_Grade) ![Node requirement](https://img.shields.io/badge/node-%3E%3D%206.14-brightgreen.svg) [![npm version](https://badge.fury.io/js/%40foeewni%2Fweb-core.svg)](https://badge.fury.io/js/%40foeewni%2Fweb-core)

# FOE EWNI Frontend Core 🌍
This repo contains the build for all the frontend core elements, the repo is split in different "bundles" and compiled through Webpack4. Refer to the *Bundles* section below

## How to build
`yarn install` then `yarn run build`

## Launching Dev
`yarn start` launches a Webpack dev server instance with HMR (hot module replacement), the dev server runs on `http://localhost:9000`, livereload listens to port 9001. 

## Bundles
The following bundles are available to ease embedding the styles and javascripts on other projects, those bundles are not to be taken as "absolute truth" as in components will vary between sites (for example one could say that forms on Campaignion are part of the critical CSS while won't be on D8 sites.)

Bundles below are for generic use, these are mostly tuned for Drupal 8 sites (main and satellites), for more granularity please import the single components on your builder.

### Critical (critical.js)
This is the most important CSS and JS bundle to deliver to the browser as soon as possible.

Embed normally as such:

```html
<link rel="stylesheet" href="/path/to/foe.critical.min.css" />
<script type="text/javascript" src="/path/to/foe.critical.min.js" />
```

Contains reset and typography, async fonts, bootstrap grid and utilities as well as hero and menu components.
This bundle priorotises over-the-fold components and most layout sizing to prevent FOUC (flash of unstyled content) as much as possible. The critical bundle is tuned specifically for our main Drupal sites.

### Main (main.js)
This is the generic bundle, this contains all other components such as forms, inputs, paragraphs, sidebars, campaign menus, etc. 

Embed with deferred/preload strategy as such:

```html
<link rel="preload" as="style" href="/path/to/foe.main.min.css" media="screen" onload="this.rel = 'stylesheet'" />
<script type="text/javascript" src="/path/to/foe.main.min.js" defer />
```

### Fonts (fonts.js)
The Fonts bundle contains a preloader for the google fonts as well as Font Awesome fonts.

### Extras (extras.js)
Ultimately the Extras bundle contains all other remaining components which are not in any way required from anonymous visitors, this bundle will append any style needed only from logged-in admins
Load as last, use server-side logic to detect logged in users.

## How to include in a project

### Javascript
Import the desired bundle from the NPM build itself, eg:
`import '@foeewni/web-core/src/XXX'` where XXX is one of the bundles above, or import as es6, eg: `import critical from '@foeewni/web-core'` 

### SCSS
For SCSS you can either import the compiled CSS from the `dist` folder (nb: you wont access the SCSS vars in this way)
Or import the actual entry files for the source SCSS, take note the global variables are all stored inside the core bundle.
Components, Drupal and Bootstrap bundles are all dependant from the core.

# Releasing

Run `npm version [major|minor|patch]` to bump the semantic versioning details within our package.json and push a commit via the usual PR/code review route (i.e. PR to `develop` branch, then PR to `master`.).  Check release here https://www.npmjs.com/package/@foeewni/web-core