const webpack = require('webpack');
const path = require('path');
const sass = require('sass');

const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin-next');
const ExtraWatchWebpackPlugin = require('extra-watch-webpack-plugin');
const LiveReloadPlugin = require('webpack-livereload-plugin');
const StyleLintPlugin = require('stylelint-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');

module.exports = (env, argv) => {
  const isProduction = argv.mode === 'production'
    || process.env.NODE_ENV === 'production';

  const plugins = [];

  // Define our dev/prod plugins
  // Ugly, yeah, but we need to load them in order for different envs.
  // (or maybe consider having 2 different config files)

  if (!isProduction) {
    plugins.push(new StyleLintPlugin({
      configFile: '.stylelintrc',
      context: './src',
      files: '**/*.(sa|sc|c)ss',
    }));
  }
  if (isProduction) {
    plugins.push(new CleanWebpackPlugin(['dist']));
  }

  plugins.push(new CopyWebpackPlugin({
    patterns: [
      {
        from: './src/assets/videos/*',
        to: 'assets/videos/[name][ext]',
      },
      {
        from: './src/assets/*',
        to: 'assets/[name][ext]',
      },
    ],
  }));

  plugins.push(new MiniCssExtractPlugin({
    // Options similar to the same options in webpackOptions.output
    // both options are optional
    filename: isProduction ? 'css/[name].min.css' : 'css/[name].css',
    chunkFilename: isProduction ? 'css/[id].min.css' : 'css/[id].css',
  }));

  if (!isProduction) {
    plugins.push(new ExtraWatchWebpackPlugin({
      files: ['./src/styleguide/**'],
    }));
  }

  if (!isProduction) {
    plugins.push(new LiveReloadPlugin({
      port: 9001,
      appendScriptTag: true,
    }));
  }

  if (!isProduction) {
    plugins.push(new WebpackShellPlugin({
      dev: true,
      onBuildExit: {
        scripts: ['yarn run styleguide'],
        blocking: true,
      },
    }));
  }

  if (!isProduction) {
    plugins.push(new webpack.HotModuleReplacementPlugin());
    plugins.push(new BundleAnalyzerPlugin({
      analyzerMode: 'static',
      openAnalyzer: false,
    }));
  }

  return {
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: [
            /\/core-js/,
          ],
          include: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules'),
          ],
          use: [
            {
              loader: 'babel-loader?cacheDirectory=true',
              options: {},
            },
          ],
        },
        {
          test: /\.(sa|sc|c)ss$/,
          include: [
            path.resolve(__dirname, 'src'),
            path.resolve(__dirname, 'node_modules'),
          ],
          use: [
            {
              loader: 'css-hot-loader',
            },
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: !isProduction,
                importLoaders: 1,
                modules: {
                  mode: 'icss',
                },
              },
            },
            {
              loader: 'sass-loader',
              options: {
                // Prefer `dart-sass`
                implementation: sass,
                sourceMap: !isProduction,
                sassOptions: {
                  outputStyle: isProduction ? 'compressed' : 'expanded',
                },
              },
            },
          ],
        },
        {
          test: /\.hb$/,
          include: [path.resolve(__dirname, 'src')],
          use: [],
        },
      ],
    },

    mode: isProduction ? 'production' : 'development',

    entry: {
      'foe.critical': './src/critical.js',
      'foe.main': './src/main.js',
      'foe.extras': './src/extras.js',
      'foe.fonts': './src/fonts.js',
    },

    output: {
      filename: isProduction ? 'js/[name].min.js' : 'js/[name].js',
      path: path.resolve(__dirname, 'dist'),
    },

    externals: {
      // treat jquery as external third party global
      // and prevent webpack to bundle it everywhere
      jquery: 'jQuery',
    },

    optimization: {
      usedExports: true,
      splitChunks: false,
      minimize: isProduction,
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            sourceMap: !isProduction,
            format: {
              comments: false,
            },
          },
          extractComments: false,
          parallel: true,
        }),
      ],
    },

    plugins, // equals to plugins: plugins, ES6 magic.

    resolve: {
      fallback: { url: require.resolve('url/') },
    },

    devServer: {
      static: {
        directory: path.join(__dirname, 'dist'),
      },
      compress: true,
      http2: true,
      https: true,
      port: 9000,
      hot: false,
      open: false,
      client: {
        overlay: true,
      },
      watchFiles: [
        'src/*',
        'src/**/*',
      ],
    },
  };
};
