import './components/typography/blockquotes.scss';
import './components/typography/metadata.scss';

// Rest of layout
import './components/layout/horizontal-rule.scss';

// Forms
import './components/form/forms.scss';
import './components/form/custom-forms.scss';

// Rest of components
import './components/campaign-navigation';
import './components/go-top';
import './components/modals';

// Now for the JS
import './components/tools/data-scroll-to';
import './components/tools/data-click-track';
import './components/tools/rubik-tracking';
import './components/tools/data-defer-src';
import './components/tools/stickystack';

// Bootstrap helpers
import './components/layout/alerts.scss';
import './components/layout/tables.scss';
import 'bootstrap/js/src/util';
import 'bootstrap/js/src/alert';
import 'bootstrap/js/src/scrollspy';

if (!window.FoeWebcore || !window.FoeWebcore.critical) {
  window.console.warn('You have loaded the main bundle without embedding the critical bundle first');
}
