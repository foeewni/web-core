import { jarallax, jarallaxElement, jarallaxVideo } from 'jarallax';
import objectFitImages from 'object-fit-images';
import Screen from '../../utils/js/screen.service';

import './style.scss';


(($) => {
  const MOBILE_REGEX = /iPad|iPhone|iPod|Android/;

  const isMobile = () => {
    const breakpoints = Screen.getBreakpoints();
    const mobileSize = parseInt(breakpoints['breakpoint-md'], 10);

    if (MOBILE_REGEX.test(navigator.userAgent) || window.innerWidth < mobileSize) {
      return true;
    }
    return false;
  };

  $(window).bind('load', () => {
    /**
     * Initialize all non-jarallax data-video-src
     * Checks for a suitable device (> 768px wide), then loads in the hero video
     */

    // If it's a mobile device don't bother loading videos
    if (isMobile()) {
      return;
    }

    objectFitImages();
    jarallaxElement();
    jarallaxVideo();

    // Initialize all Jarallax first
    jarallax(document.querySelectorAll('.jarallax'), {
      speed: 0.2,
      disableParallax: isMobile(),
      disableVideo: isMobile(),
      videoVolume: 0,
      videoPlayOnlyVisible: true,
    });

    // Bail out if visibility change API is not supported
    if (typeof document.hidden === 'undefined'
     && (typeof document.msHidden === 'undefined'
     || typeof document.webkitHidden === 'undefined')) {
      return;
    }

    // Added visibility change API, ugly af but copied straight from MDN.
    // Set the name of the hidden property and the change event for visibility
    let hiddenMethod;
    let visibilityChange;
    if (typeof document.hidden !== 'undefined') { // Opera 12.10 and Firefox 18 and later support
      hiddenMethod = 'hidden';
      visibilityChange = 'visibilitychange';
    } else if (typeof document.msHidden !== 'undefined') {
      hiddenMethod = 'msHidden';
      visibilityChange = 'msvisibilitychange';
    } else if (typeof document.webkitHidden !== 'undefined') {
      hiddenMethod = 'webkitHidden';
      visibilityChange = 'webkitvisibilitychange';
    }

    $('[data-video-src]').each((index, el) => {
      // Bail out if this element is a jarallax-enabled el
      if ($(el).hasClass('jarallax')) {
        return;
      }

      const videoEl = document.createElement('video');
      videoEl.setAttribute('autoplay', 'autoplay');
      videoEl.setAttribute('loop', 'loop');
      videoEl.setAttribute('preload', 'auto');
      videoEl.setAttribute('muted', 'muted');
      videoEl.setAttribute('volume', '0');

      const videoSource = document.createElement('source');
      videoSource.setAttribute('src', el.getAttribute('data-video-src'));
      videoEl.appendChild(videoSource);

      el.appendChild(videoEl);

      // For browsers that don't respect the muted attribute set above
      videoEl.muted = true;

      // If the page is hidden, pause the video;
      // if the page is shown, play the video
      const handleVisibilityChange = () => {
        if (document[hiddenMethod]) {
          videoEl.pause();
        } else {
          videoEl.play();
        }
      };

      const checkElementVisible = () => {
        if (videoEl.paused === true && Screen.elementInViewport(videoEl)) {
          videoEl.play();
        }
        if (videoEl.paused === false && !Screen.elementInViewport(videoEl)) {
          videoEl.pause();
        }
      };

      $(document).on(visibilityChange, handleVisibilityChange);
      $(window).on('scroll', checkElementVisible);
    });
  });
})(window.jQuery);
