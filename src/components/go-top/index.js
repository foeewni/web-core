/* @docs

# Scripts:Core/Go Top

Creates an arrow and shows it at a scrolled distance of the same amount of 2 times the user's viewport height to bring the user back to the top, the component gets injected on the page automatically if the
`body` tag contains the class `foe-page-go-top`. You can see enabled on this documentation page.

Usage:
```
<body class="foe-page-go-top"></body>
```
*/
import Screen from '../../utils/js/screen.service';

import './style.scss';

(($) => {
  if ($('body').hasClass('foe-page-go-top')) {
    let threshold = Screen.getScrollFromScreens(2);
    const $anchor = $('<a name="foe-go-top-anchor"></a>');
    const $gotop = $(`
    <a id="foe-go-top" href="#foe-go-top-anchor" title="Return to the top of the page">
      <i class="fas fa-arrow-alt-circle-up"></i>
      <span class="sr-only">Go to top</span>
    </a>
    `);

    $anchor.prependTo('body');
    $gotop.appendTo('body');

    const showHideArrow = () => {
      const scroll = Screen.getScrollTop();
      if (scroll >= threshold && !$gotop.hasClass('visible')) {
        $gotop.addClass('visible');
      }

      if (scroll < threshold && $gotop.hasClass('visible')) {
        $gotop.removeClass('visible');
      }
    };

    $(window).on('scroll', showHideArrow);
    $(window).on('resize', () => {
      threshold = Screen.getScrollFromScreens(2);
      showHideArrow();
    });
  }
})(window.jQuery);
