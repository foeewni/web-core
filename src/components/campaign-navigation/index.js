import './style.scss';

(($) => {
  const $menu = $('#foe-secondary-nav');
  const $nav = $('#campaignnav-menu-items');

  function campaignNavToggleClass() {
    if (!$nav.hasClass('collapsing')) {
      $menu.toggleClass('nav-open');
      $nav.toggleClass('collapse');
    }
  }

  $(document).on('click', '#js-foe-secondary-trigger', campaignNavToggleClass);
})(window.jQuery);
