/* @docs

# Scripts:Core/Scroll to

Checks for either for hash `#` anchor or `data-scroll-to` attribute on the markup and uses them on links/CTAs/elements which need to scroll the page to a certain target.
The argument must be a jQuery selectable object or an anchor with the `name` attribute (eg: `#id-selector`, `.class-selector`, `[attribute-selector=value]`)
In some cases we want to open a share window and also scroll to a new location for when the user returns.
For this use case we use the target `_share` as follows <a href="https://facebook.com" target="_share" data-scroll-to="#core">Example: open facebook and also scroll to core section on this page</a>

Usage:
```
<a href="#sub-section">Animate to subsection (using anchor)</a>
<button class="btn btn-primary" data-scroll-to=".donation-form">Animate to donation form (using class selector)</button>
```

*/

import screenService from '../../utils/js/screen.service';

// Enables smooth scrolling between anchors
(($) => {
  function onLoad() {
    // Prevent default jump scroll on page load
    if (window.location.hash) {
      setTimeout(() => {
        window.scrollTo(0, 0);
      }, 1);

      // Simulate a click on the in-page link
      setTimeout(() => {
        $(`[href="${window.location.hash}"]`).click();
      }, 1);
    }
  }

  function scrollFunction(event) {
    const { hash } = this;
    let allowDefaultAnchorBehaviour = false;
    // Bail out if we shouldn't be "scrolling-to" on this click
    // For certain pages we open a new target to an action and we also want the page to scroll eg scrolling thank you pages
    // So if there is a data-scroll-to we should continue with the scroll whatever else is happening
    if (this.tagName === 'A') {
      if ($(this).attr('data-scroll-to')) {
        allowDefaultAnchorBehaviour = true;
      } else if (this.target === '_blank'
          || window.location.hostname !== this.hostname
          || window.location.pathname.replace(/^\//, '') !== this.pathname.replace(/^\//, '')
          || (hash === '' && !this.dataset.scrollTo)
          || this.search !== window.location.search) {
        return;
      }
    }

    // Get the target from the hash
    let $target = /#.{2,}/.test(hash) ? $(hash) : $($(this).attr('data-scroll-to'));
    $target = $target.length ? $target : $(`[name="${hash.substr(1)}"]`);

    if ($target.length) {
      // Header when scrolled is always compressed, unless if the page is at the very start
      // if we grab the height at the start of the document, it will be bigger than what
      // it will be when the header is compressed.

      // Grab the value from our scss variables
      const headerHeight = screenService.getHeaderHeight();

      // See if the target specifies a scroll padding, useful for elements without an internal padding
      const extraPadding = parseInt($target.attr('data-scroll-to-padding') || 20, 10);

      let finalScrollPosition = ($target.offset().top - (headerHeight + extraPadding));

      // Compensate admin bars if user is logged in
      if ($('body').hasClass('user-logged-in')) {
        const adminSelectors = ['#toolbar-bar', '#toolbar-item-administration-tray', '.tabs--primary'];
        adminSelectors.forEach((selector) => {
          if ($(selector).length) {
            finalScrollPosition -= $(selector).height();
          }
        });
      }

      $('html, body')
        .stop()
        .animate(
          { scrollTop: finalScrollPosition }, 750, 'swing'
        );

      // Prevent default jump scroll
      if (!allowDefaultAnchorBehaviour) {
        event.preventDefault();
      }
    }
  }

  $(document).on('click', 'a:not([data-scroll-to])', scrollFunction);
  $(document).on('click', '[data-scroll-to]', scrollFunction);
  $(document).on('load', onLoad);
})(window.jQuery);
