/* @donotdocs

# Scripts:Core/Scrollspy

Catches the user browser history as it navigates through our sites.
NB: The file is called "rubik" as it feeds to our internal project all the user data it needs.

Script is bundled in core.js, just import it normally and should work it out the box.

*/

// Foe lightweight implementation of the scrollspy bootstrap plugin
// 'cause it doesn't work out of the box and it doesn't spit any warnings/errors so eh, sod it
(($) => {
  // Cache selectors
  let lastId = null;
  const $menu = $('.foe-scrollspy');
  const $links = $menu.find('a');

  function mapItems() {
    const item = $($(this).attr('href'));
    if (item.length) {
      return item;
    }
    return null;
  }

  const $items = $links.map(mapItems);

  function scrollTrigger() {
    // Get container scroll position, but we detect when the ID gets to the middle of the screen
    // to compensate the short sections at bottom and the narrow footer
    // PS, actually would be good to have a bit of breathing space below
    const fromTop = $(this).scrollTop() + (window.innerHeight / 2);
    function currentScrollItem() {
      if ($(this).offset().top < fromTop) {
        return this;
      }
      return null;
    }
    // Get id of current scroll item
    const currScroll = $items.map(currentScrollItem);
    // Get the id of the current element
    const cur = currScroll[currScroll.length - 1];
    const id = (cur && cur.length) ? cur[0].id : '';

    if (lastId !== id) {
      lastId = id;
      // Set/remove active class
      $links
        .parent()
        .removeClass('active')
        .end()
        .filter(`[href="#${id}"]`)
        .parent()
        .addClass('active');

      // Update fragment
      if (id) {
        window.history.replaceState(null, null, `#${id}`);
      }
    }
  }

  // Bind to scroll
  $(window).on('load scroll', scrollTrigger);
})(window.jQuery);
