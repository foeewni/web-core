/* @docs

# Scripts:Core/Sticky Stack

Vanilla Javascript implementation allowing multiple sticky elements, accounting for horizontal offsets and with options for "semi" stickyness, and mobile skipping.

###### Usage

Apply appropriate styles to the elements you want to be involved with the stickyness:
* `js-stickystack` -- Elements that should become sticky instead of scrolling offscreen
* `js-stickystack-top` -- The element whose top edge should be considered the point at which stickyness begins. This allows external toolbars and other elements that may have other stickyness to remain visible. (Can be applied to an element without the `js-stickystack` class.)

###### Modifiers

Elements with the `js-stickystack`can be further modified by adding the classes below.
* `js-stickystack-semi` -- Elements that will stick until pushed offscreen by another sticky element
* `js-stickystack-nomobile` -- Elements that will not be sticky on smaller screens. (Currently hard-coded to <= 768px.)

*/

/* @preserve
 *
 * StickyStack
 *
 * A vanilla javascript sticky-element stacker
 *
 * For licence details and usage see:
 * https://github.com/jrsouth/stickystack
 *
 */

import Screen from '../../utils/js/screen.service';

(() => {
  // Kill script if we are below IE11 or Apple devices
  if (window.navigator.userAgent.indexOf('MSIE') >= 0
    || /iPad|iPhone|iPod/.test(navigator.userAgent)
    || !!navigator.userAgent.match(/Trident.*rv:11\./)) {
    return;
  }

  const breakpoints = Screen.getBreakpoints();

  const StickyStack = {
    mobileBreak: parseInt(breakpoints.filter(bp => bp.size === 'md').pop().width, 10),
    list: [],
    number: 0,
    stackTop: 0,
    pageBottom: null,

    init: () => {
      StickyStack.stopObserving();
      // Clean up if init already run
      if (StickyStack.pageBottom) {
        StickyStack.pageBottom.parentNode.removeChild(StickyStack.pageBottom);
      }

      for (let i = 0; i < StickyStack.number; i++) {
        const { placeholder, element } = StickyStack.list[i];

        placeholder.parentNode.removeChild(placeholder);
        StickyStack.removeClass(element, 'stickystack-stuck');

        element.style.position = null;
        element.style.top = null;
        element.style.left = null;
        element.style.width = null;
        element.style.marginTop = null;
        element.style.marginBottom = null;
      }

      // Create element to push bottom of the page up
      // <div style="height:0;width:100%;" class="js-stickystack"></div>

      StickyStack.pageBottom = document.createElement('div');
      StickyStack.pageBottom.style.height = 0;
      StickyStack.pageBottom.style.width = '100%';
      StickyStack.addClass(StickyStack.pageBottom, 'js-stickystack');

      document.body.appendChild(StickyStack.pageBottom);

      // Reset StickyStack.number
      StickyStack.number = 0;

      // Get elements with 'js-stickystack' class
      let elementList = document.getElementsByClassName('js-stickystack');

      // If on a small screen, strip out elements marked
      // to _not_ stick on mobile (js-stickystack-nomobile)
      // TODO: Dynamically load/assign mobile breakpoint
      if (window.innerWidth <= StickyStack.mobileBreak) {
        const newElementList = [];
        for (let j = 0; j < elementList.length; j++) {
          if (!StickyStack.hasClass(elementList[j], 'js-stickystack-nomobile')) {
            newElementList.push(elementList[j]);
          }
        }
        elementList = newElementList;
      }

      StickyStack.number = elementList.length;
      StickyStack.list = [];
      StickyStack.stackTop = 0;

      for (let j2 = 0; j2 < StickyStack.number; j2++) {
        // Get element's computed style
        // Used both to set up the placeholder and to get needed list[] values
        const style = window.getComputedStyle(elementList[j2]);

        // Create placeholder block element, replicating all
        // layout-affecting properties of the sticky element
        const placeholderElement = document.createElement('div');
        placeholderElement.style.width = style.getPropertyValue('width');
        placeholderElement.style.height = style.getPropertyValue('height');
        placeholderElement.style.top = style.getPropertyValue('top');
        placeholderElement.style.right = style.getPropertyValue('right');
        placeholderElement.style.bottom = style.getPropertyValue('bottom');
        placeholderElement.style.left = style.getPropertyValue('left');
        placeholderElement.style.margin = style.getPropertyValue('margin');
        placeholderElement.style.padding = style.getPropertyValue('padding');
        placeholderElement.style.border = style.getPropertyValue('border');
        placeholderElement.style.float = style.getPropertyValue('float');
        placeholderElement.style.clear = style.getPropertyValue('clear');
        placeholderElement.style.position = style.getPropertyValue('position');

        // Hide the placeholder until it's needed
        // And have it invisible even when it IS needed
        placeholderElement.style.display = 'none';
        placeholderElement.style.visibility = 'hidden';

        // Add it to the DOM, immediately before the sticky element
        elementList[j2].parentNode.insertBefore(placeholderElement, elementList[j2]);

        const coords = StickyStack.getCoords(elementList[j2]);

        StickyStack.list.push({
          element: elementList[j2],
          top: coords.top,
          left: coords.left - parseFloat(style.getPropertyValue('margin-left')),
          width: parseFloat(style.getPropertyValue('width')),
          placeholder: placeholderElement,
          semistuck: StickyStack.hasClass(elementList[j2], 'js-stickystack-semi'),
          zIndex: 100 - j2,
        });
      }

      // Sort the list top down
      StickyStack.list.sort(StickyStack.sortByTop);

      // Set the stack top to (the top of) the highest-placed element
      // with the js-stickystack-top class. Note that this element
      // does not have to be sticky itself (otherwise we could just use the
      // existing vertically-sorted list).
      //
      // This allows other sticky elements (admin toolbars etc.) to
      // peacefully co-exist with StickyStack.
      //
      // Defaults to 0 (the top of the viewport) if no elements are found.

      StickyStack.stackTop = 0;

      elementList = document.getElementsByClassName('js-stickystack-top');
      for (let j3 = 0; j3 < elementList.length; j3++) {
        const { top } = StickyStack.getCoords(elementList[j3]);
        if (j3 === 0) {
          StickyStack.stackTop = top;
        } else {
          StickyStack.stackTop = Math.min(StickyStack.stackTop, top);
        }
      }

      // Fire the initial calculation
      StickyStack.update();
      StickyStack.startObserving();
    },

    update: function update() {
      const stack = [[StickyStack.stackTop, 0, document.body.offsetWidth]];
      const pageOffset = window.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop || 0;

      for (let i = 0; i < StickyStack.number; i++) {
        const curr = StickyStack.list[i];
        const {
          element,
          placeholder,
          left,
          width,
        } = curr;
        const top = curr.top - pageOffset;
        const overlapData = StickyStack.getOverlap([top, left, width], stack);

        if (overlapData.overlap > 0) {
          placeholder.style.display = 'block';

          StickyStack.addClass(element, 'stickystack-stuck');

          let scrollback = 0;

          // Semi-sticky handling

          // Only bother if this element is 'semistuck' and there are
          // further elements in the list.
          if (StickyStack.list[i].semistuck && i + 1 < StickyStack.number) {
            const tempStack = [[overlapData.absolute + element.offsetHeight, left, width]];

            // Check each following element for collision/overlap
            for (let j = i + 1; j < StickyStack.number; j++) {
              const testBlock = [
                StickyStack.list[j].top - pageOffset,
                StickyStack.list[j].left,
                StickyStack.list[j].width,
              ];
              const overlapSemiData = StickyStack.getOverlap(testBlock, tempStack);

              if (overlapSemiData.overlap > 0) {
                scrollback = overlapSemiData.overlap;
                break;
              }
            }
          }

          element.style.position = 'fixed';
          element.style.top = `${overlapData.absolute - scrollback}px`;
          element.style.left = `${left}px`;
          element.style.width = `${width}px`;
          element.style.zIndex = curr.zIndex;
          element.style.marginTop = 0;
          element.style.marginBottom = 0;
          const overlapTop = (overlapData.absolute + element.offsetHeight) - scrollback;

          stack.push([
            overlapTop,
            element.offsetLeft,
            element.offsetWidth,
          ]);
        } else {
          StickyStack.removeClass(element, 'stickystack-stuck');

          element.style.position = null;
          element.style.top = null;
          element.style.left = null;
          element.style.width = null;
          element.style.zIndex = null;
          element.style.marginTop = null;
          element.style.marginBottom = null;

          placeholder.style.display = 'none';
        }
      }
    },

    startObserving: function startObserving() {
      // If there's no existing MutationObserver object, create one to
      // trigger a recalulation on changes in the sticky elements.
      // If there is one, stop it.
      // if (!StickyStack.observer) {
      //     var MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
      //     StickyStack.observer = new MutationObserver(StickyStack.init);
      // } else {
      //     StickyStack.observer.disconnect();
      // }

      // Since any change to the document might require a re-layout, observe EVERYTHING
      // TODO: Check performance penalty, if any.
      // StickyStack.observer.observe(document.body, {
      //     attributes: true,
      //     characterData: true,
      //     subtree: true
      // });
    },

    stopObserving: function stopObserving() {
      // if (StickyStack.observer) {
      //     StickyStack.observer.disconnect();
      // }
    },

    getOverlap: function getOverlap(block, stack) {
      let overlap = 0;
      let absolute = 0;
      for (let i = 0; i < stack.length; i++) {
        if (block[0] < stack[i][0] && block[1] + block[2] > stack[i][1] && stack[i][1] + stack[i][2] > block[1]) {
          overlap = Math.max(overlap, stack[i][0] - block[0]);
          absolute = Math.max(absolute, stack[i][0]);
        }
      }
      return { overlap, absolute };
    },

    sortByTop: function sortByTop(a, b) {
      return a.top - b.top;
    },

    getCoords: function getCoords(el) {
      // From http://stackoverflow.com/a/26230989

      const box = el.getBoundingClientRect();

      const { body, documentElement } = document;

      const scrollTop = window.pageYOffset || documentElement.scrollTop || body.scrollTop;
      const scrollLeft = window.pageXOffset || documentElement.scrollLeft || body.scrollLeft;

      const clientTop = documentElement.clientTop || body.clientTop || 0;
      const clientLeft = documentElement.clientLeft || body.clientLeft || 0;

      const top = (box.top + scrollTop) - clientTop;
      const left = (box.left + scrollLeft) - clientLeft;

      return { top: Math.round(top), left: Math.round(left) };
    },

    // Helper-functions pilfered wholesale from
    // http://jaketrent.com/post/addremove-classes-raw-javascript/

    hasClass: function hasClass(el, className) {
      if (el.classList) {
        return el.classList.contains(className);
      }
      return !!el.className.match(new RegExp(`(\\s|^)${className}(\\s|$)`));
    },

    addClass: function addClass(el, className) {
      if (el.classList) {
        el.classList.add(className);
      } else if (!StickyStack.hasClass(el, className)) {
        el.className += ` ${className}`;
      }
    },

    removeClass: function removeClass(el, className) {
      if (el.classList) {
        el.classList.remove(className);
      } else if (StickyStack.hasClass(el, className)) {
        const reg = new RegExp(`(\\s|^)${className}(\\s|$)`);
        el.className = el.className.replace(reg, ' ');
      }
    },

  };

  // Set up listeners to fire on load
  window.addEventListener('DOMContentLoaded', StickyStack.init);
  window.addEventListener('load', StickyStack.init);
  window.addEventListener('resize', StickyStack.init);

  // Set up listeners to fire on scroll
  window.addEventListener('scroll', StickyStack.update);
  window.addEventListener('touchmove', StickyStack.update);

  // Add a hacky empty function to cause iOS to update the scroll offset value while scrolling
  // Still doesn't update during momentum scrolling
  // From https://remysharp.com/2012/05/24/issues-with-position-fixed-scrolling-on-ios/
  // TODO: Check if needed now that touchmove event has update() attached
  window.ontouchstart = () => {};

  // Trigger delayed extra recalculations, in case elements change
  // size as a result of the first init() call.
  window.addEventListener('DOMContentLoaded', () => {
    setTimeout(() => {
      StickyStack.update();
    }, 500);
  });
  window.addEventListener('load', () => {
    setTimeout(() => {
      StickyStack.update();
    }, 500);
  });
})();
