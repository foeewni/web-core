/* @docs

# Scripts:Core/Iframe Deferrer

Use the <code>data-defer-src</code> attribute on iframes to defer/lazy load adding a
helper loading class.

Usage:
```
<iframe data-defer-src="<target url>"></iframe>
<iframe data-defer-src="<target url>" data-defer-dispose />
```

```html_example
<div>
  <p>Native <code>loading="lazy"</code>, not widely supported on iframes.</p>
  <div class="mb-4 embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" src="https://player.vimeo.com/video/291108273" loading="lazy" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  </div>

  <p>Using <code>data-defer-src</code>.</p>
  <div class="mb-4 embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" data-defer-src="https://player.vimeo.com/video/291108273" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  </div>

  <p>Combining with <code>data-defer-dispose</code> to free up resources when iframe goes out of the screen.</p>
  <div class="embed-responsive embed-responsive-16by9">
    <iframe class="embed-responsive-item" data-defer-dispose data-defer-src="https://player.vimeo.com/video/291108273" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
  </div>
</div>
```

*/

import './data-defer-src.scss';

(() => {
  window.deferSrc = (el) => {
    let isVisible = false;
    const source = el.dataset.deferSrc;

    el.classList.add('js-defer-is-loading');

    // Remove class on load.
    el.addEventListener('load', () => {
      el.classList.remove('js-defer-is-loading');
    });

    const observer = new IntersectionObserver((entries) => {
      entries.forEach((entry) => {
        if (!isVisible && entry.intersectionRatio > 0) {
          isVisible = true;
          if (el.tagName === 'IFRAME') {
            el.contentWindow.location.replace(source);
          } else {
            el.src = source;
          }
        }

        if (el.hasAttribute('data-defer-dispose') && isVisible && entry.intersectionRatio === 0) {
          isVisible = false;
          if (el.tagName === 'IFRAME') {
            el.contentWindow.location.replace('about:blank');
          } else {
            el.removeAttribute('src');
          }
          el.classList.add('js-defer-is-loading');
        }
      });
    });

    observer.observe(el);
  };

  const elements = document.querySelectorAll('[data-defer-src]');

  elements.forEach((el) => {
    window.deferSrc(el);
  });
})();
