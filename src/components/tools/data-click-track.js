/* @docs

# Scripts:Core/Click track

Adds or updates `data-click-track` attributes for all navigation links,
tracking happens automatically by adding this javascript on the page,
however the attribute can be customised to send arbritrary data to GA.

Append a track object on the attribute as such:

```
{
  "category": "Custom category",
  "action": "Custom action",
  "label": "Custom label",
}
```

Usage:
```
<a class="btn btn-primary" href="https://...">Automatically tracked outgoing button link</a>

<a class="btn btn-primary" href="#internal" data-click-track="{\"category\": \"Custom category\", \"action\": \"Custom action\", \"label\": \"Custom label\",}">Internal tracked link</a>
```
*/

(($) => {
  const REGEX_FILES = /(\.jpg|\.gif|\.png|\.pdf|\.doc|\.docx|\.zip|\.tar|\.gz|\.rar|\.wav|\.mp4|\.giv|\.webm)$/gi;

  // Quick object merging function
  // Duplicated keys in 'second' will clobber values in 'first'
  function smush(first, second) {
    Object.keys(second).forEach((key) => { first[key] = second[key]; });
    return first;
  }

  // Compute a suitable eventAction from available information
  function extrapolateAction(link) {
    const host = link.hostname;
    const { href } = link;

    let action = null;
    if (host === window.location.hostname
        || host.match(/.*foe\.co\.uk/)
        || host.match(/.*foeshop\.co\.uk/)
        || host.match(/.*friendsoftheearth\.uk/)
        || host.match(/.*takeclimateaction\.uk/)
    ) {
      if (host.indexOf('act') !== -1) {
        // Is it campaignion?
        action = 'Click to Campaignion';
      } else if (href.match(REGEX_FILES)) {
        action = 'download-resource';
      } else {
        action = 'Click inbound';
      }
    } else {
      action = 'Click outbound';
    }

    return action;
  }

  // Compute a suitable eventLabel from available information
  function extrapolateLabel(link) {
    return `${link.textContent.trim()} | ${link.href}`;
  }

  // Compute a suitable eventCategory from available information
  function extrapolateCategory(link) {
    const tag = link.tagName;
    let category = 'unknown';

    if (tag === 'A') {
      if (link.className.match(/(foe-cta|btn)/)) {
        // Assume it's a cta
        category = 'Link CTA';
      } else {
        category = 'Link Text';
      }
    }

    return category;
  }

  function extrapolateData() {
    let dataClickTrackAttributes = {};
    if (this.dataset.clickTrack) {
      try {
        dataClickTrackAttributes = JSON.parse(this.dataset.clickTrack);
      } catch (e) {
        console.warn('Warning: [data-click-track] attribute is not valid', e);
      }
    }

    const extrapolatedAttributes = {
      eventCategory: extrapolateCategory(this),
      eventAction: extrapolateAction(this),
      eventLabel: extrapolateLabel(this),
    };

    this.dataset.clickTrack = JSON.stringify(smush(extrapolatedAttributes, dataClickTrackAttributes));
  }

  $(document).ready(() => {
    $('a[href]').each(extrapolateData);
  });
})(window.jQuery);
