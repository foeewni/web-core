/* @docs

# Scripts:Core/Rubik Tracking

Catches the user browser history as it navigates through our sites. The file is called "rubik" because it feeds data to our internal data warehouse project of that name. The script is bundled with core.js, just import it normally and it should work out of the box.

You can test locally by visiting:

<ul>
  <li>
    <a href="?source=FN123456&utm_campaign=trees&utm_source=facebook&utm_medium=paid-social&utm_content=FY2021-10_lm123123123_nonsense&utm_term=TBH%20we%20rarely%20use%20term&refsid=1234&o=other%20value&tags=one,two&decache=123#scripts-core-rubik-tracking">A simulated paid-social link with a <code>source</code> parameter</a>
  </li>
  <li>
    <a href="?o=alternative%20key%20for%20other&tags=three&decache=456#scripts-core-rubik-tracking">A link with some other parameters</a>
  </li>
</ul>

After each page load the stored data should be visible at: <strong>Application &gt; Storage &gt; Local Storage &gt;</strong> <code>foe_data</code> within your browser devtools.
In addition, the source_code_first is available as a cookie, and source_code_session as the entry source code for that session
There are dummy values provided for most of the <code>&lt;meta&gt;</code> tags within the Styleguide template file, so those test URLs should populate everything except the <code>external_referrer</code>.

*/

import * as store from 'local-storage';

// Please refrain from naming keys or strings with keywords such as "ad", "ads", "tracking" as some AdBlockers might detect them.
// Obfuscation however should hide variable naming
(($) => {
  const DATA_STRUCTURE_VERSION = 3;

  const HISTORY_LIMIT = 10;
  const SOURCE_HISTORY_LIMIT = 10;
  const ANALYTICS_SESSION_LENGTH = (30 * 60 * 1000); // expire the session in 30 minutes (30 mins * 60 seconds * 1000 milliseconds)

  function sortUnique(array) {
    if (!array.length) {
      return array;
    }
    const arraySorted = array.sort((a, b) => ((a >= b) ? 1 : -1));
    const result = [arraySorted[0]];
    for (let i = 1; i < arraySorted.length; i++) {
      if (arraySorted[i - 1] !== arraySorted[i]) {
        result.push(arraySorted[i]);
      }
    }
    return result;
  }


  const parameterStructure = {
    // Object representing the desired data structure
    //  - Keys are... keys
    //  - Object values indicate nested structure
    //  - String values map to query string parameter keys
    //  - Array values indicate an ordered preference of query string parameter keys
    //    followed by an optional transformation function

    analytics: {
      source: 'utm_source',
      medium: 'utm_medium',
      term: 'utm_term',
      content: 'utm_content',
      campaign: 'utm_campaign',
    },

    internal: {
      source: ['source', x => x && x.toUpperCase()],
      source_code_first: ['source', (newData, currentData) => {
        if (currentData && typeof currentData === 'string') {
          return currentData.toUpperCase();
        }
        if (newData && typeof newData === 'string') {
          return newData.toUpperCase();
        }
        return null;
      }],
      mailing: ['utm_content', (x) => {
        // Format is FYY1Y2-MM_lm123456_description-of-placement
        // We want:           [--------]
        // Optional section, pushed to upper case
        if (x) {
          const parts = x.split('_');
          if (parts[1] && parts[1].indexOf('lm') === 0) {
            return parts[1].toUpperCase();
          }
        }
        return null;
      }],
      activity_year: ['utm_content', (x) => {
        // Format is FYY1Y2-MM_lm123456_description-of-placement
        // We want:   [----]
        // With Y1 and Y2 separated by a forward slash
        if (x) {
          if (x.match(/^FY\d\d\d\d/)) {
            return x.replace(/^FY(\d\d)(\d\d).*$/, '$1/$2');
          }
        }
        return null;
      }],
      activity_month: ['utm_content', (x) => {
        // Format is FYY1Y2-MM_lm123456_description-of-placement
        // We want:        [--]
        if (x) {
          if (x.match(/^FY\d\d\d\d-\d\d/)) {
            return x.replace(/^FY\d\d\d\d-(\d\d).*$/, '$1');
          }
        }
        return null;
      }],
    },

    other: ['other', 'o'],
    refsid: 'refsid',
    tags: ['tags', (newData, currentData) => {
      if (newData) {
        return sortUnique($.merge(currentData, newData.split(',')));
      }
      return currentData || [];
    },
    ],

  };


  // Custom recursive deep-copy (ish) function ¯\_(ツ)_/¯
  const getParameters = (structure, parameters, currentData) => {
    let result;

    switch (typeof structure) {
      case 'string':
        // It's a simple string.
        // Return the new parameter value, or the previous parameter value, or null
        if (structure in parameters) {
          return parameters[structure];
        }
        return typeof currentData === 'string' ? currentData : null;


      case 'object':
        // It's either an Array or an Object

        if (Array.isArray(structure)) {
        // It's an array

          // Iterate through candidate parameter keys, quitting once a value is found
          for (let i = 0, l = structure.length; i < l; i++) {
            if (typeof structure[i] === 'string' && structure[i] in parameters) {
              result = parameters[structure[i]];
              break;
            }
          }

          // If the last element of the array is a function, call it on the interim result
          if (typeof structure[structure.length - 1] === 'function') {
            result = structure[structure.length - 1](result, currentData);
          }

          // Return it, or the previous result, or null
          if (!(result == null)) {
            return result;
          }
          return typeof currentData === 'string' ? currentData : null;
        }

        // Otherwise it's a (nested) object, so iterate over its keys.
        // Hooray for recursion...?
        result = {};

        Object.keys(structure).forEach((key) => {
          const value = getParameters(structure[key], parameters, currentData[key] || {});
          if (!(value == null)) {
            // Excludes both null and undefined, but allows false-y values like zero and the various empties
            result[key] = value;
          } else {
            // Set an explicit null where we don't have a value.
            // May not be needed in future.
            result[key] = null;
          }
        });

        return result;

      default:
        // It's a... something else??? ABORT
        return null;
    }
  };

  function setAttribution(structure, parameters) {
    // retrieve just the current analytics parameters
    const attribution = getParameters(structure, parameters, {});

    // if there is analytics data set that here
    if ((Object.entries(attribution)
      .filter(([, v]) => v !== null)).length) {
      Object.assign(attribution, {
        expiry: Date.now() + ANALYTICS_SESSION_LENGTH,
      });
      store.set('foe_attribution', attribution);
      return attribution;
    }

    // if the session is stale discard it
    const attributionSession = store.get('foe_attribution');
    if (attributionSession) {
      if (attributionSession.expiry < Date.now()) {
        store.set('foe_attribution', null);
        return null;
      }
      Object.assign(attributionSession, {
        expiry: Date.now() + ANALYTICS_SESSION_LENGTH,
      });
      store.set('foe_attribution', attributionSession);
    }

    return attributionSession;
  }

  function populateForm(data) {
    // Prepare the JSON string
    const jsonData = JSON.stringify(data);

    // Inject it for submission alongside the form
    const $prefill = $('input[data-form-prefill-write="foe_additional_data"]');
    const $legacy = $('input[name="submitted[foe_additional_data]"]') || $('input[name="foe_additional_data"]');

    if ($prefill.length) {
      $prefill.val(jsonData);
    } else {
      $legacy.val(jsonData);
    }

    // Inject the first source code that we've seen for this user ever
    const $sourceCodeFirst = $('input[data-form-prefill-write="source_code_first"]');
    if ($sourceCodeFirst.length && typeof data.internal.source_code_first === 'string') {
      $sourceCodeFirst.val(data.internal.source_code_first);
    }

    // and populate source_code_session from sessionStorage - check it exists before getting it
    if (typeof sessionStorage.source_code_session !== 'undefined') {
      const sessionStorageSourceCodeSession = sessionStorage.getItem('source_code_session');
      const sourceCodeSession = $('input[data-form-prefill-write="source_code_session"]');
      if (sessionStorageSourceCodeSession !== null
        && sessionStorageSourceCodeSession.length
        && typeof sessionStorageSourceCodeSession === 'string') {
        sourceCodeSession.val(sessionStorageSourceCodeSession);
      }
    }

    // and populate analytic_session from sessionStorage - check it exists before getting it
    const attribution = store.get('foe_attribution');
    if (attribution
      && typeof attribution === 'object') {
      Object.keys(attribution).forEach((key) => {
        const attributionInput = $(`input[data-form-prefill-write="attribution_${key}"]`);
        if (attribution[key]
          && attribution[key].length
          && typeof attribution[key] === 'string') {
          attributionInput.val(attribution[key]);
        }
      });
    }
  }

  // Adds the param to a named array (if needed)
  // and trims the array to a desired maximum (if provided).
  function historyAdd(array, value, limit) {
    if (array[0] !== value) {
      array.unshift(value);
    }
    if (Number.isInteger(limit) && array.length > limit) {
      // eslint-disable-next-line no-param-reassign
      array = array.slice(0, limit);
    }
    return array;
  }

  function getURLParameters() {
    const parameters = {};
    const variables = window.location.search.substring(1).split('&');
    for (let i = 0; i < variables.length; i++) {
      const parameter = variables[i].split('=');
      const key = parameter[0];
      const value = parameter[1];

      parameters[key] = decodeURIComponent(value);
    }
    return parameters;
  }

  /* eslint-disable */
  // http://x443.wordpress.com/2012/03/18/adler32-checksum-in-javascript/
  // I'll disable eslint because I have no clue how to ES6ify the absurd assignment within the for cycle
  function newUserID() {
    const adler32 = (a, b, c, d, e, f) => {
      for (b = 65521, c = 1, d = e = 0; f = a.charCodeAt(e++); d = (d + c) % b) c = (c + f) % b;
      return (d << 16) | c;
    };
    return adler32(String(Math.random() + Date.now()));
  }
  /* eslint-enable */


  function getMeta(metaName) {
    const metas = document.getElementsByTagName('meta');
    for (let i = 0; i < metas.length; i++) {
      if (metas[i].getAttribute('name') === metaName) {
        return metas[i].getAttribute('content');
      }
    }
    return null;
  }


  function run() {
    // this is getting foe-data local storage object
    const trackingData = Object.assign({
      history: [],
      tags: [],
      source_history: [],
    }, store.get('foe_data'));
    trackingData.user_id = trackingData.user_id || newUserID();

    const parameters = getURLParameters();

    // Bloody IE...
    const baseURL = window.location.origin || `${window.location.protocol}//${window.location.hostname}`;

    // Get new parameters, passing the current params in to fill in blanks
    const newParameterData = getParameters(parameterStructure, parameters, trackingData);
    Object.assign(trackingData, newParameterData);

    // External referrer
    if (typeof trackingData.external_referer === 'undefined') {
      if (document.referrer.indexOf(baseURL) !== 0) {
        trackingData.external_referer = document.referrer;
      } else if (typeof parameters.external_referer !== 'undefined'
          && parameters.external_referer.indexOf(baseURL) !== 0) {
        trackingData.external_referer = parameters.external_referer;
      }
    }

    // FOE Campaign and topic, extracted from <meta> tags
    trackingData.internal = trackingData.internal || {};
    trackingData.internal.topic = getMeta('foe:topic');
    trackingData.internal.campaign = getMeta('foe:campaign');
    trackingData.internal.page_type = getMeta('foe:page-type');

    // Drupal node ID
    trackingData.nid = $('[data-history-node-id]').attr('data-history-node-id')
      || $('[data-nid]').attr('data-nid')
      || $('[id*="node"]').attr('id')
      || null;
    if (typeof trackingData.nid === 'string') {
      trackingData.nid = trackingData.nid.replace('node-', '');
    }

    // Source from form if needed
    /* If visitor arrives on the site randomly without a source code in the url ie not from social media,
     and navigate to a donation form, take the code from that donation form and store that as their first one */
    // ie if Not source code first...then fill with form value
    const $sourceCode = $('input[data-form-prefill-write="source_code"]');
    if ($sourceCode.length) {
      if (typeof trackingData.internal.source_code_first === 'undefined') {
        trackingData.internal.source_code_first = $sourceCode.val();
      }
      if (typeof trackingData.internal.source === 'undefined') {
        // use baked-in source from form
        trackingData.internal.source = $sourceCode.val();
      }
    }

    // URL history
    trackingData.history = historyAdd(trackingData.history, window.location.href, HISTORY_LIMIT);

    // typeof checks its defined and not set to type undefined
    if (typeof parameters.source === 'string') {
      // add source and timestamp to history
      const sourceHistoryObj = { source: parameters.source, timestamp: Date.now() };
      trackingData.source_history = historyAdd(trackingData.source_history, sourceHistoryObj, SOURCE_HISTORY_LIMIT);
    }

    // Set the data structure variable
    trackingData.version = DATA_STRUCTURE_VERSION;

    // also set source_code_first as a cookie
    document.cookie = `source_code_first=${trackingData.internal.source_code_first};`;
    // set source_code_session as session storage - seems to be set to null string initially for some reason
    // check trackingData.internal.source has a value before trying to set it!
    if (trackingData.internal.source !== null
        && (typeof sessionStorage.source_code_session === 'undefined'
          || sessionStorage.source_code_session === 'null')) {
      sessionStorage.setItem('source_code_session', trackingData.internal.source);
    }

    setAttribution(parameterStructure.analytics, parameters);

    populateForm(trackingData);

    // set local storage
    store.set('foe_data', trackingData);
  }

  $(document).ready(run);
})(window.jQuery);
