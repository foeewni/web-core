/* @preserve
 *
 * Concept readapted from MenuStack https://github.com/jrsouth/MenuStack
 *
 */

(() => {
  const MenuStack = {
    elements: [],
    resizeTimer: null,
    init: () => {
      // Prevents init if we have already elements.
      if (MenuStack.elements.length > 0) {
        return false;
      }

      const elements = document.querySelectorAll('.js-menustack');

      for (let i = 0; i < elements.length; i++) {
        const thisEl = elements[i];

        const thisElStyle = window.getComputedStyle(thisEl);

        let pseudoOffset = parseInt(window.getComputedStyle(thisEl, ':before').getPropertyValue('top'), 10);
        const needsPlaceholder = thisElStyle.getPropertyValue('position') !== 'fixed';
        let finalOffset = thisEl.offsetTop;

        // IE and Safari reports it as "auto" instead of the proper value
        // when parsed to Int it converts into NaN
        if (Number.isNaN(pseudoOffset)) {
          pseudoOffset = 0;
        }
        finalOffset += pseudoOffset;

        let placeholderElement = false;
        if (needsPlaceholder) {
          placeholderElement = document.createElement('div');
          MenuStack.addClass(placeholderElement, 'js-menustack-placeholder');

          placeholderElement.style.width = thisElStyle.getPropertyValue('width');
          placeholderElement.style.height = thisElStyle.getPropertyValue('height');
          placeholderElement.style.top = thisElStyle.getPropertyValue('top');
          placeholderElement.style.right = thisElStyle.getPropertyValue('right');
          placeholderElement.style.bottom = thisElStyle.getPropertyValue('bottom');
          placeholderElement.style.left = thisElStyle.getPropertyValue('left');
          placeholderElement.style.margin = thisElStyle.getPropertyValue('margin');
          placeholderElement.style.padding = thisElStyle.getPropertyValue('padding');
          placeholderElement.style.border = thisElStyle.getPropertyValue('border');
          placeholderElement.style.float = thisElStyle.getPropertyValue('float');
          placeholderElement.style.clear = thisElStyle.getPropertyValue('clear');
          placeholderElement.style.position = thisElStyle.getPropertyValue('position');

          // Hide the placeholder until it's needed
          // And have it invisible even when it IS needed
          placeholderElement.style.display = 'none';
          placeholderElement.style.visibility = 'hidden';

          // Add it to the DOM, immediately before the sticky element
          thisEl.parentNode.insertBefore(placeholderElement, thisEl);
        }

        MenuStack.elements.push({
          dom: thisEl,
          offsetTop: finalOffset,
          placeholder: placeholderElement,
        });
      }

      // In case the window is somewhere else.
      MenuStack.update();
      return true;
    },
    update: () => {
      const pageOffset = window.scrollY || window.pageYOffset
        || document.documentElement.scrollTop || document.body.scrollTop || 0;
      for (let i = 0; i < MenuStack.elements.length; i++) {
        const thisEl = MenuStack.elements[i].dom;
        const thisOffsetTop = MenuStack.elements[i].offsetTop;
        const thisPlaceholder = MenuStack.elements[i].placeholder;

        if (pageOffset > thisOffsetTop) {
          if (!MenuStack.hasClass(thisEl, 'js-menustack-stuck')) {
            MenuStack.addClass(thisEl, 'js-menustack-stuck');
            if (thisPlaceholder) {
              thisPlaceholder.style.display = 'block';
            }
          }
        } else {
          MenuStack.removeClass(thisEl, 'js-menustack-stuck');
          if (thisPlaceholder) {
            thisPlaceholder.style.display = 'none';
          }
        }
      }
    },
    hasClass: function hasClass(el, className) {
      if (el.classList) {
        return el.classList.contains(className);
      }
      return !!el.className.match(/(\\s|^)' + className + '(\\s|$)/g);
    },

    addClass: function addClass(el, className) {
      if (el.classList) {
        el.classList.add(className);
      } else if (!MenuStack.hasClass(el, className)) {
        el.className += ` ${className}`;
      }
    },

    removeClass: function removeClass(el, className) {
      if (el.classList) {
        el.classList.remove(className);
      } else if (MenuStack.hasClass(el, className)) {
        const reg = /(\\s|^)' + className + '(\\s|$)/g;
        el.className = el.className.replace(reg, ' ');
      }
    },

    resize: () => {
      // Safe destroy it if we have a pending timer.
      if (MenuStack.resizeTimer !== null) {
        clearTimeout(MenuStack.resizeTimer);
        MenuStack.resizeTimer = null;
      }

      // Recreate the timer
      MenuStack.resizeTimer = setTimeout(() => {
        MenuStack.flush();
        MenuStack.init();
      }, 250);
    },

    flush: () => {
      // Clear everything
      if (MenuStack.elements.length === 0) {
        return false;
      }

      for (let i = 0; i < MenuStack.elements.length; i++) {
        const thisEl = MenuStack.elements[i];
        if (MenuStack.hasClass(thisEl.dom, 'js-menustack-stuck')) {
          MenuStack.removeClass(thisEl.dom, 'js-menustack-stuck');
        }
        if (thisEl.placeholder) {
          thisEl.placeholder.parentNode.removeChild(thisEl.placeholder);
        }
      }

      MenuStack.elements = [];
      return true;
    },
  };

  // Set up listeners to fire on load
  window.addEventListener('DOMContentLoaded', MenuStack.init);
  window.addEventListener('load', MenuStack.init);
  window.addEventListener('resize', MenuStack.resize);

  // Set up listeners to fire on scroll
  window.addEventListener('scroll', MenuStack.update);
  window.addEventListener('touchmove', MenuStack.update);

  // Add a hacky empty function to cause iOS to update the scroll offset value while scrolling
  // Still doesn't update during momentum scrolling
  // From https://remysharp.com/2012/05/24/issues-with-position-fixed-scrolling-on-ios/
  // TODO: Check if needed now that touchmove event has update() attached
  window.ontouchstart = () => {};
})();
