import './menustack';

// import 'bootstrap/js/src/collapse';
import Popper from 'popper.js';
import Screen from '../../utils/js/screen.service';

import './style.scss';

(($) => {
  const $foeheader = $('.foe-header');

  function menuSearchClose(event) {
    const $menu = $('.header-search');
    if (event === 'force' || event.keyCode === 27) {
      $menu.removeClass('open');
    }
    return false;
  }

  function menuMobileToggler() {
    const $nav = $(this.dataset.target);
    const $menu = $nav.closest('.foe-header');
    if (!$nav.hasClass('collapsing')) {
      $menu.toggleClass('foe-header-mobile-is-open');

      if ($menu.hasClass('foe-header-mobile-is-open')) {
        menuSearchClose('force');
      }
    }
  }

  function menuSearchSwitching(event) {
    event.preventDefault();
    const $menu = $(this).parent();
    const $input = $menu.find('input');

    if ($menu.hasClass('open')) {
      $menu.removeClass('open');
    } else {
      $menu.addClass('open');
      $input.focus();
    }

    if ($foeheader.hasClass('foe-header-mobile-is-open')) {
      menuMobileToggler();
    }

    return false;
  }

  function menuMultilevelRemove() {
    // Remove any poppers currently in place
    $('ul.foe-multi-level-menu ul.dropdown-menu').each((index, element) => {
      if (typeof element.popper === 'object' && typeof element.popper.destroy === 'function') {
        element.popper.destroy();
      }
    });
    $('ul.foe-multi-level-menu').removeClass('foe-multi-level-menu-active');
  }

  function menuMultilevelSetup() {
    const breakpoints = Screen.getBreakpoints();
    const allBreakpoints = [];
    Object.keys(breakpoints).forEach((key) => {
      if (key.indexOf('breakpoint') === 0) {
        const size = key.replace('breakpoint-', '');
        allBreakpoints.push(size);
      }
    });

    const currentBreakpoint = Screen.getCurrentBreakpoint();
    const activeBreakpoints = allBreakpoints.slice(0, allBreakpoints.indexOf(currentBreakpoint) + 1);

    // Clear up any residual popper.js behaviour
    menuMultilevelRemove();

    // Add it back, per menu, if needed at the current breakpoint
    $('ul.foe-multi-level-menu').each((index, menu) => {
      let expandBreakpoint = allBreakpoints[0];

      // Look for a parent element with a navbar-expand-xyz class
      const menuParent = $(menu).closest('[class*="navbar-expand-"]');
      if (menuParent.length === 1) {
        expandBreakpoint = menuParent[0].className.replace(/^.*navbar-expand-([a-z]*).*$/, '$1');
      }

      // If we're not at the right breakpoint, bail out
      if (activeBreakpoints.indexOf(expandBreakpoint) === -1) {
        return;
      }

      $(menu).addClass('foe-multi-level-menu-active');

      // Add popper.js to second level menus (First dropdown level)
      $(menu).find('> li.nav-link').each((i, firstLevelItem) => {
        $(firstLevelItem).find('> ul.dropdown-menu').each((j, secondLevelMenu) => {
          secondLevelMenu.popper = new Popper(firstLevelItem, secondLevelMenu, {
            placement: 'bottom',
            originalPlacement: 'bottom',
          });
          // Add popper.js to third-level menus (second dropdown/dropside level)
          $(secondLevelMenu).find('> li.dropdown-parent').each((k, secondLevelItem) => {
            $(secondLevelItem).find('> ul.dropdown-menu').each((l, thirdLevelMenu) => {
              thirdLevelMenu.popper = new Popper(secondLevelItem, thirdLevelMenu, {
                placement: 'right-start',
                originalPlacement: 'right-start',
              });
            });
          });
        });
      });
    });
  }

  // Listeners
  $(menuMultilevelSetup);
  $(window).resize(menuMultilevelSetup);
  $(document).on('click', '#js-foe-navbar-toggle', menuMobileToggler);
  $(document).on('click', '.js-foe-menu-search-button', menuSearchSwitching);
  $('.header-search input').keyup(menuSearchClose);
})(window.jQuery);
