import * as critical from './critical';
import * as main from './main';
import * as components from './components';
import * as fonts from './fonts';
import * as extras from './extras';

export default {
  critical,
  main,
  components,
  fonts,
  extras,
};
