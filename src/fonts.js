/* @docs

# Core:Typography/Fonts

Our type system is comprised of two fonts:
* [Libre Franklin](https://fonts.google.com/specimen/Libre+Franklin)
* [Libre Baskerville](https://fonts.google.com/specimen/Libre+Baskerville)

<div class="row">
  <div class="col">
    <h4>Our primary typeface is Libre Franklin</h4>
    <p>It’s a geometric <b>sans serif</b> with organic features. It’s our primary typeface for <b>body copy</b> and <b>titles</b>.</p>
  </div>
  <div class="col" style="font-family:'Libre Baskerville';">
    <h4>Our secondary typeface is Libre Baskerville</h4>
    <p>It’s a <b>high-contrast serif</b> that’s designed for screen. It’s our complementary typeface for <b>titles</b> and <b>callouts</b>.</p>
  </div>
</div>

To use them on your CSS please import the webfonts through Google Fonts

```
<link id="fonts" rel="stylesheet" crossorigin="anonymous" href="https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital@0;1&family=Libre+Franklin:ital,wght@0,400;0,700;1,400;1,700&display=swap" />
```

@TODO: Document Fontawesome

*/


// Import fontawesome icons
// Note, single line deep imports are needed here, for some reason fontawesome messes up
// with normal imports and webpack ends up bundling the whole library (1.25mb!)
import { library, dom } from '@fortawesome/fontawesome-svg-core';

import { faEnvelope } from '@fortawesome/free-solid-svg-icons/faEnvelope';
import { faSearch } from '@fortawesome/free-solid-svg-icons/faSearch';
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes';
import { faSyncAlt } from '@fortawesome/free-solid-svg-icons/faSyncAlt';
import { faExternalLinkSquareAlt } from '@fortawesome/free-solid-svg-icons/faExternalLinkSquareAlt';
import { faArrowAltCircleUp } from '@fortawesome/free-solid-svg-icons/faArrowAltCircleUp';
import { faShare } from '@fortawesome/free-solid-svg-icons/faShare';
import { faLink } from '@fortawesome/free-solid-svg-icons/faLink';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons/faChevronDown';
import { faChevronUp } from '@fortawesome/free-solid-svg-icons/faChevronUp';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons/faAngleDown';
import { faNewspaper } from '@fortawesome/free-solid-svg-icons/faNewspaper';
import { faClock } from '@fortawesome/free-solid-svg-icons/faClock';
import { faEye } from '@fortawesome/free-solid-svg-icons/faEye';
import { faCloudDownloadAlt } from '@fortawesome/free-solid-svg-icons/faCloudDownloadAlt';
import { faFile } from '@fortawesome/free-solid-svg-icons/faFile';
import { faDownload } from '@fortawesome/free-solid-svg-icons/faDownload';
import { faFilter } from '@fortawesome/free-solid-svg-icons/faFilter';
import { faInfo } from '@fortawesome/free-solid-svg-icons/faInfo';
import { faShareAlt } from '@fortawesome/free-solid-svg-icons/faShareAlt';
import { faAngleDoubleDown } from '@fortawesome/free-solid-svg-icons/faAngleDoubleDown';
import { faAngleDoubleUp } from '@fortawesome/free-solid-svg-icons/faAngleDoubleUp';
import { faLocationArrow } from '@fortawesome/free-solid-svg-icons/faLocationArrow';
import { faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons/faMapMarkedAlt';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons/faMapMarkerAlt';
import { faCloudUploadAlt } from '@fortawesome/free-solid-svg-icons/faCloudUploadAlt';
import { faCog } from '@fortawesome/free-solid-svg-icons/faCog';
import { faSave } from '@fortawesome/free-solid-svg-icons/faSave';
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons/faSignInAlt';
import { faTrashAlt } from '@fortawesome/free-solid-svg-icons/faTrashAlt';

import { faTwitter } from '@fortawesome/free-brands-svg-icons/faTwitter';
import { faXTwitter } from '@fortawesome/free-brands-svg-icons/faXTwitter';
import { faFacebook } from '@fortawesome/free-brands-svg-icons/faFacebook';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons/faFacebookF';
import { faPinterest } from '@fortawesome/free-brands-svg-icons/faPinterest';
import { faLinkedin } from '@fortawesome/free-brands-svg-icons/faLinkedin';
import { faInstagram } from '@fortawesome/free-brands-svg-icons/faInstagram';
import { faYoutube } from '@fortawesome/free-brands-svg-icons/faYoutube';
import { faTumblr } from '@fortawesome/free-brands-svg-icons/faTumblr';
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons/faWhatsapp';
import { faFacebookMessenger } from '@fortawesome/free-brands-svg-icons/faFacebookMessenger';
import { faTelegram } from '@fortawesome/free-brands-svg-icons/faTelegram';
import { faTelegramPlane } from '@fortawesome/free-brands-svg-icons/faTelegramPlane';
import { faTiktok } from '@fortawesome/free-brands-svg-icons/faTiktok';
import { faBluesky } from '@fortawesome/free-brands-svg-icons/faBluesky';
import { faThreads } from '@fortawesome/free-brands-svg-icons/faThreads';


// Add actually used FA icons
library.add(
  faEnvelope,
  faSearch,
  faTimes,
  faSyncAlt,
  faExternalLinkSquareAlt,
  faArrowAltCircleUp,
  faShare,
  faLink,
  faTwitter,
  faXTwitter,
  faFacebook,
  faFacebookF,
  faPinterest,
  faLinkedin,
  faInstagram,
  faYoutube,
  faTumblr,
  faWhatsapp,
  faFacebookMessenger,
  faTelegram,
  faTelegramPlane,
  faTiktok,
  faChevronDown,
  faChevronUp,
  faAngleDown,
  faNewspaper,
  faClock,
  faEye,
  faCloudDownloadAlt,
  faFile,
  faDownload,
  faFilter,
  faInfo,
  faShareAlt,
  faAngleDoubleDown,
  faAngleDoubleUp,
  faLocationArrow,
  faMapMarkerAlt,
  faMapMarkedAlt,
  faCog,
  faSave,
  faTrashAlt,
  faCloudUploadAlt,
  faSignInAlt,
  faBluesky,
  faThreads
);

// Look for Google fonts and append them if they are not there already
// We avoiding using @import url(); because the new URL has semicolons on it
// and it tremendously gets broken from some preprocessors.

const fonts = window.document.createElement('link');
// eslint-disable-next-line max-len
const href = 'https://fonts.googleapis.com/css2?family=Libre+Baskerville:ital@0;1&family=Libre+Franklin:ital,wght@0,400;0,700;1,400;1,700&display=swap';
fonts.setAttribute('rel', 'stylesheet');
fonts.setAttribute('crossorigin', 'anonymous');
fonts.setAttribute('href', href);
document.head.appendChild(fonts);

// Kicks off the process of finding <i> tags and replacing with <svg>
dom.watch();
