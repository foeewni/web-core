import './components/hero-panel';
import './components/navigation';
import './components/campaign-navigation';
import './components/go-top';

// eslint-disable-next-line max-len
window.console.warn('Importing components.js is deprecated and will be removed in future versions, import each component directly in your build or use the new bundles.');
