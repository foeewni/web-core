// Import our main scss here
import './components/layout/reset.scss';

// Core layout and typography
import './components/layout/grid.scss';
import './components/layout/images.scss';
import './components/layout/backgrounds.scss';

import './components/typography/headings.scss';
import './components/typography/display.scss';
import './components/typography/paragraphs.scss';
import './components/hero-panel';
import './components/form/buttons.scss';
import './components/navigation';

window.FoeWebcore = {
  critical: true,
};

// CSS Preloader Polyfills for IE11/Firefox

if (!document.createElement('link').relList || !document.createElement('link').relList.supports('preload')) {
  const links = document.querySelectorAll('link[rel=preload]');
  for (let index = 0; index < links.length; index++) {
    const link = links[index];
    const type = link.getAttribute('as');
    if (type === 'style') {
      link.removeAttribute('onload');
      link.setAttribute('rel', 'stylesheet');
    } else {
      window.console.warn('Only <link rel=preload as=style /> tags are supported at the moment');
    }
  }
}
