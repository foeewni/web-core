import scss from '../scss/exporter.scss';

export default {
  getScrollTop: function getScrollTop() {
    if (typeof window.pageYOffset !== 'undefined') {
      // most browsers except IE before #9
      return window.pageYOffset;
    }
    const B = document.body; // IE 'quirks'
    let D = document.documentElement; // IE with doctype
    D = (D.clientHeight) ? D : B;
    return D.scrollTop;
  },

  getHalfScrollTop: function getHalfScrollTop() {
    return this.getScrollTop() + (window.innerHeight / 2);
  },

  getDocumentHeight: function getDocumentHeight() {
    const { body } = document;
    const html = document.documentElement;

    return Math.max(body.scrollHeight, body.offsetHeight, html.clientHeight, html.scrollHeight, html.offsetHeight);
  },

  getScrollFromScreens: function getScrollFromScreens(screens) {
    const height = this.getViewportHeight();

    return height * screens;
  },


  getScrollFromPerc: function getScrollFromPerc(perc) {
    const height = this.getDocumentHeight();

    return height / 100 * perc;
  },

  getViewportHeight: function getViewportHeight() {
    return Math.max(document.documentElement.clientHeight, window.innerHeight);
  },

  // @todo: legacy function, replace with intersection observer
  // after assessing how many things rely on this utility.
  elementInViewport: function elementInViewport(el) {
    const $ = window.jQuery;
    const { top } = $(el).offset();
    const height = el.clientHeight;
    const bottom = top + height;
    const scroll = this.getScrollTop();
    const viewHeight = this.getViewportHeight();
    return !(bottom < scroll || top - viewHeight >= scroll);
  },

  getBreakpoints: function getBreakpoints() {
    // For additional instructions see:
    // ./src/utils/scss/exporter.scss
    const breakpoints = [];
    // When used as dependency it exports scss.default
    const exportedScss = scss.default || scss;
    Object.keys(exportedScss).forEach((key) => {
      if (key.indexOf('breakpoint') === 0) {
        const size = key.replace('breakpoint-', '');
        const width = parseInt(exportedScss[key], 10);
        breakpoints.push({ size, width });
      }
    });

    return breakpoints;
  },

  getCurrentBreakpoint: function getCurrentBreakpoint() {
    const breakpoints = this.getBreakpoints();

    let breakpointIndex = 0;
    while (breakpointIndex < (breakpoints.length - 1) && window.innerWidth >= breakpoints[breakpointIndex + 1].width) {
      breakpointIndex += 1;
    }

    return breakpoints[breakpointIndex].size;
  },

  getHeaderHeight: function getHeaderHeight() {
    const $ = window.jQuery;
    const breakpoint = this.getCurrentBreakpoint();

    if ($('.foe-header').length) {
      return parseInt(scss[`foe-header-height-${breakpoint}`], 10);
    }
    return 0;
  },
};
